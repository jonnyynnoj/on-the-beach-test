import React, { useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronDown, faStar } from '@fortawesome/free-solid-svg-icons';

const Card = styled.div`
    background: white;
    margin-bottom: 1.6rem;
`;

const Content = styled.div`
    display: flex;
    position: relative;
`;

const Image = styled.img`
    max-width: 60%;
`;

const Info = styled.article`
    flex: 1;
    font-size: 0.8rem;
    padding: 1rem;
`;

const Name = styled.h2`
    color: rgb(62, 83, 149);
    font-size: 1.2rem;
    margin: 0 0 0.25rem;
`;

const Location = styled.div`
    color: #868686;
`;

const Stars = styled.div`
    color: rgb(254, 220, 7);
    margin: 0.8rem 0;
`;

const Star = styled(FontAwesomeIcon)`
    margin-right: 2px;
`;

const Specs = styled.p`
    color: #494949;
    line-height: 1.8em;
`;

const BookButton = styled.a`
    background: rgb(254,220,7);
    border: 0;
    border-radius: 5px;
    color: rgb(62, 83, 149);
    cursor: pointer;
    display: block;
    font-weight: bold;
    padding: 1em;
    text-align: center;
    text-decoration: none;

    .price {
        font-weight: bold;
        font-size: 1.2rem;
    }
`;

const ReadMore = styled.a`
    background: white;
    color: rgb(62, 83, 149);
    cursor: pointer;
    position: absolute;
    bottom: 0;
    left: 0;
    padding: 0.4em 1em;
    font-size: 0.8em;
`;

const ReadMoreIcon = styled(FontAwesomeIcon)`
    margin-left: 10px;
`;

const Description = styled.div`
    ${props => !props.open && 'display: none;'}
    font-size: 0.7em;
    padding: 1em;
    line-height: 1.8em;

    h3 {
        color: rgb(62, 83, 149);
        font-weight: bold;
        margin: 0 0 0.8em;
    }
`;

export default ({ hotel }) => {
    const [showDescription, setShowDescription] = useState(false);

    return (
        <Card>
            <Content>
                <Image src={`img/${hotel.image}`} />
                
                <Info>
                    <Name>{hotel.name}</Name>
                    <Location>{hotel.location}</Location>

                    <Stars>
                        {Array(hotel.stars).fill().map((_, i) => (
                            <Star key={i} icon={faStar} />
                        ))}
                    </Stars>

                    <Specs>
                        <strong>{hotel.guests.adults}</strong> Adults,{' '}
                        <strong>{hotel.guests.children}</strong> children

                        {hotel.guests.infants > 0 && (
                            <span>
                                ,{' '}<strong>{hotel.guests.infants}</strong> infant
                            </span>
                        )}

                        <br/>
                        <strong><time>{hotel.date.start}</time></strong>, for <strong>{hotel.date.days}</strong> days<br />
                        departing from <strong>{hotel.departure}</strong>
                    </Specs>
                    <BookButton>
                        Book now
                        <div className="price">£{hotel.price.toLocaleString('en', { minimumFractionDigits: 2 })}</div>
                    </BookButton>
                </Info>

                <ReadMore onClick={() => setShowDescription(!showDescription)}>
                    <strong>Read more</strong> about this hotel
                    <ReadMoreIcon icon={showDescription ? faChevronDown : faChevronRight} />
                </ReadMore>
            </Content>
            
            <Description open={showDescription}>
                <h3>Overview</h3>
                <p>{hotel.description}</p>
            </Description>
        </Card>
    );
};