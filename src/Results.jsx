import React, { useState } from 'react';
import styled from 'styled-components';
import Filters, { sortOptions } from './Filters';
import HotelCard from './HotelCard';

const ResultsPage = styled.div`
    background: url('./img/background.png');
    min-height: 100vh;
    padding: 4% 0;
`;

const Container = styled.div`
    display: flex;
    margin: 0 auto;
    max-width: 1200px;
    width: 75%;
`;

const Nav = styled.nav`
    flex: 1;
    margin-right: 8%;
`;

const List = styled.div`
    flex: 3;
`;

export default ({ hotels }) => {
    const [sortBy, setSortBy] = useState(sortOptions[1]);

    return (
        <ResultsPage>
            <Container>
                <Nav>
                    <Filters currentSort={sortBy} onSelect={setSortBy} />
                </Nav>
                <List>
                    {[...hotels]
                        .sort(sortBy.func)
                        .map(hotel => <HotelCard key={hotel.name} hotel={hotel} />)}
                </List>
            </Container>
        </ResultsPage>
    );
};