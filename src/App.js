import React from 'react';
import Results from './Results';
import hotels from './hotels.json';

export default () => <Results hotels={hotels} />;
