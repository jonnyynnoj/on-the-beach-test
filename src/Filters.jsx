import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortAlphaDown, faPoundSign, faStar } from '@fortawesome/free-solid-svg-icons';

const List = styled.ul`
    font-size: 0.9em;
    list-style: none;
    margin: 0;
    padding: 0;
`;

const Option = styled.li`
    background: ${props => props.selected ? 'rgb(23, 49, 127)' : 'white'};
    color: ${props => props.selected ? 'white' : 'rgb(62, 83, 149)'};
    border: 1px solid rgb(140, 153, 192);
    border-left: 0;
    border-right: 0;
`;

const Link = styled.a`
    cursor: pointer;
    display: block;
    padding: 5%;
`;

const Icon = styled(FontAwesomeIcon)`
    color: ${props => props.selected ? 'white' : 'rgb(212, 212, 212)'};
    float: right;
`;

export const sortOptions = [
    {
        label: <span>sort <strong>alphabetically</strong></span>,
        icon: faSortAlphaDown,
        func: (a, b) => a.name.localeCompare(b.name)
    },
    {
        label: <span>sort by <strong>price</strong></span>,
        icon: faPoundSign,
        func: (a, b) => a.price - b.price
    },
    {
        label: <span>sort by <strong>star rating</strong></span>,
        icon: faStar,
        func: (a, b) => b.stars - a.stars
    }
];

export default ({ currentSort, onSelect }) => (
    <List>
        {sortOptions.map((sortOption, i) =>
            <Option key={i} selected={currentSort === sortOption}>
                <Link onClick={() => onSelect(sortOption)}>
                    {sortOption.label}
                    <Icon icon={sortOption.icon} />
                </Link>
            </Option>)
        }
    </List>
);